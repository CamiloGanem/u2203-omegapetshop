import "./App.css";
import NavPage from "./components/home/navPage";
import CatPag from "./components/home/catPage";

function App() {
  return (
    <div>
      <NavPage />
      <CatPag />
    </div>
  );
}

export default App;
