const mongoose = require('mongoose');

const productoSchema = new mongoose.Schema({
    nombre: {type: String, require: true},
    precio: {type: Number, require: true},
    descripcion: {type: String, require: true},
    foto: {type: String, require: true},
    cantidad: {type: Number, require: true},
    calificacion: {type: Number},
    tipo_mascota: {type: mongoose.Types.ObjectId, rel: 'mascotas'}
},{
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('producto', productoSchema)