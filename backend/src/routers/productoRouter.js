const express = require('express');
const router = express.Router();
const ctrProducto= require('../controllers/productoController');


router.post('/producto', ctrProducto.guardar);//create
router.get('/producto',ctrProducto.listar);//read
router.delete('/producto:id',ctrProducto.eliminar);//delete
router.put('/producto:id',ctrProducto.actualizar);//update
//router.get('/producto/prducto/perfil',ctrProducto.productoPerfil);//read

//router.get('/perfiles', ctrProducto.perfiles);//Read
//router.get('/producto/id:id',ctrProducto.buscarId);//Read
//router.get('/producto/nombre/:nom',ctrProducto.buscar);//Read
  
module.exports = router;