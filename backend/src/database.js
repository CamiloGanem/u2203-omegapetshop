const mongoose = require('mongoose');

mongoose
.connect(process.env.MONGODB_URI)
.then(bd => console.log('Conecto con la base de datos'))
.catch(err =>console.log('Error en DB' + err))