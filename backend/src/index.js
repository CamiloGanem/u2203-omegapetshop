require('dotenv').config(); //llamda a la dependencia para usar las varibales de entorno (.env)
require('./database'); //llamada al archivo que hace la conexion con la base de datos (database.js)
require('./models/producto'); //llamada al modelo de producto para crear el squema

const express = require("express"); //instancia de express 
const app = express(); //asignacion de express a la constante app
const port = 9000; //puerto de conexion
const productoRouter = require('./routers/productoRouter'); //asignamos a una variable la clase productoRouter

//Utilidades
app.use(express.json());

//Rutas
app.use('/', productoRouter);


/*
ejemplo get con express
app.get('/:nom', (req, res) => {   // servicio rest get 
    res.send('Hello World ' + (req.params.nom * 5))
})*/

app.listen(port, (req, res) =>{ //escuhar las peticiones get, post, put, delete
    console.log('Servidor en linea');
})