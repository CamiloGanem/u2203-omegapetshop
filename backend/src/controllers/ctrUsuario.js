
ctrProducto.buscarId = async(req, res) =>{
    let id = req.params.id
    await producto
    .findById(id)
    .then(data => res.json(data))
    .catch(err =>res.json(err))
}

ctrProducto.perfiles = async(req, res) =>{
    await perfil
    .find()
    .then(data => res.json(data))
    .catch(err =>res.json(err))
}

ctrProducto.productoPerfil = async(req, res)=>{
    await producto
    .find().populate('perfil')
    .then(data => res.json(data))
    .catch(err =>res.json(err))
}