const ctrProducto = {};
const producto = require('../models/producto');

//Metodos del CRUD
ctrProducto.guardar = async (req, res) =>{
    await producto
    .create(req.body)
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

ctrProducto.listar = async (req, res) =>{
    await producto.find()
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

ctrProducto.actualizar = async (req, res) => {
    let param = req.params.id;
    await producto
    .updateOne({_id:param})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

ctrProducto.eliminar = async (req, res) =>{
    const {_id, ...body} = req.body;
    await producto
    .deleteOne({_id:param}, {$set : body})
    .then(data => res.json(data))
    .catch(err => res.json(err))
}

module.exports = ctrProducto;